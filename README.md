# Student Developers Society's homepage
[![pipeline status](https://gitlab.com/studevsoc/infra/home/badges/main/pipeline.svg)](https://gitlab.com/studevsoc/infra/home/-/commits/main) 

This is a minimal website made with [Flask](https://flask.palletsprojects.com/).
This is where people who look us up on the internet come to. Therefore we need
to avoid downtime.

### General Guidelines
- **Keep things minimal**: Add features conservatively. Don't write an extra line of
  code than what is necessary.
- **Look into the standard library before looking elsewhere**: Do not introduce
  unnecessary dependencies to the code.  Try and stick to the standard library
  for the most part because it's well audited and secure. Using a library from
  the standard library also let's us keep the web app's overall footprint small.
- **Refactor your code**: When possible refactor before pushing otherwise refactor
  after pushing. Refactor. A feature is not complete without refactoring.

### Questions
**Q: Found a bug? Want to add a feature? Any question at all?**

A: Open an issue.

### License
[GNU Affero General Public License v3.0](https://gitlab.com/studevsoc/infra/home/-/raw/main/LICENSE) all the way.
